package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Component;

public class Coin extends Component {
    public Coin(String name, Integer value) {
        super(name, value);
    }
}
