package ulco.cardGame.common.players;

import ulco.cardGame.common.games.boards.Coin;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PokerPlayer extends BoardPlayer {
    List<Card>cards;
    List<Coin>coins;
    public PokerPlayer(String name) {
        super(name);
        this.cards = new ArrayList<>();
    }

    @Override
    public Integer getScore() {
        return null;
    }

    @Override
    public Component play() {
        Card pokerToPlay = (Card)cards.get(0);

        this.removeComponent(pokerToPlay);

        return pokerToPlay;

    }

    @Override
    public void addComponent(Component component) {
        // Using instanceof keywork
        if (component instanceof Card)
            cards.add((Card)component);
            this.score = cards.size();
    }

    @Override
    public void removeComponent(Component component) {
        cards.remove(component);

        // current player score (cards in hand)
        this.score = cards.size();
    }

    @Override
    public List<Card> getComponents() {
        this.cards = new ArrayList<>();
       // return null;
        return this.cards;
    }
    
    public List<Component> getSpecificComponents(Class classType ){ return null;}

    @Override
    public void shuffleHand() {

        Collections.shuffle(cards);
    }

    @Override
    public void clearHand() {

        for (Component card : cards) {
            card.setPlayer(null);
        }

        this.cards = new ArrayList<>();
    }
    public void displayHand(){
        
    }
    public String toString() {
        return "PokerPlayer{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
