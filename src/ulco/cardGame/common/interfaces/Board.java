package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.util.List;

public interface Board {
     void clear();
     void addComponent(Component component);
     List<Component> getComponent();

     List<Component> getSpecificComponents(Class classType);

     void displayState();


}
